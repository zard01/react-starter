/* eslint-disable tailwindcss/no-custom-classname */
/* eslint-disable @next/next/no-img-element */
import React from 'react'
import Link from 'next/link'
function index() {
    return (
        <div className="container mx-auto w-full min-h-screen bg-red-200 shadow-2xl">
            <header className="flex items-center w-full h-16 bg-green-200 shadow">
                <img className="pl-3" src="https://picsum.photos/50/50" alt="logo" />
                <h1 className="px-3">This is title</h1>
            </header>
            <section>
                <nav>
                    <div className="flex float-right flex-wrap space-x-4">
                        <button className="p-2 text-center text-black hover:text-white bg-gray-200 hover:bg-gray-600 rounded-md" ><Link href="20210725test/about"><a>about</a></Link></button>
                        <button className="p-2 text-center text-black hover:text-white bg-gray-200 hover:bg-gray-600 rounded-md" ><Link href="20210725test/news"><a>news</a></Link></button>
                        <button className="p-2 text-center text-black hover:text-white bg-gray-200 hover:bg-gray-600 rounded-md" ><Link href="20210725test/portfolio"><a>portfolio</a></Link></button>
                        <button className="p-2 text-center text-black hover:text-white bg-gray-200 hover:bg-gray-600 rounded-md" ><Link href="20210725test/contact"><a>contact</a></Link></button>
                        <button className="p-2 text-center text-black hover:text-white bg-gray-200 hover:bg-gray-600 rounded-md" ><Link href=" "><a>home</a></Link></button>
                    </div>
                </nav>
            </section>
            <section className="p-2">
                <h1 className="pt-3 text-xl text-black">index</h1>
                <div className="grid grid-cols-3 gap-4 w-full">
                    <div className="text-center bg-gray-200">1</div>
                    <div>2</div>
                    <div>3</div>
                    <div>4</div>
                    <div>5</div>
                    <div>6</div>
                </div>
            </section>
        </div>
    )
}

export default index

